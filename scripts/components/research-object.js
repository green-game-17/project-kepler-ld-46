AFRAME.registerComponent('research-object', {
  schema: {
    type: { type: 'string', default: 'text' },
    id: { type: 'string', default: '' },
    attribute: { type: 'string', default: '' }
  },
  init () {
    selectSystem('research').registerListener(this.el, this.data.type, this.data.id)
    this.el.addEventListener('newValue', event => {
      if (this.data.attribute === '') {
        this.el.setAttribute(event.detail.attribute, translate(event.detail.value))
      } else {
        this.el.setAttribute(this.data.attribute, translate(event.detail))
      }
    })
  }
})