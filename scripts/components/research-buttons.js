AFRAME.registerComponent('research-buttons', {
  init () {
    this.distances = {
      x: 0.1855,
    }
    this.activeButton = null

    let colors = {
      on: '#4d4f3b',
      off: '#0a0a07'
    }

    this.buttonEls = []

    for (let research in researchCatalogue) {
      let x = (this.distances.x * this.buttonEls.length)
      let button = createEntity(this.el, {
        id: 'research' + research + 'Button',
        position: x + ' 0.01 0' ,
        depth: '0.14',
        height: '0.033',
        width: '0.14',
        color: colors.off
      }, 'a-box')
      let icon = createEntity(button, {
        id: 'research' + research + 'ButtonText',
        position: '0 0.017 0',
        rotation: '-90 0 0',
        scale: '0.1 0.1 0.1',
        align: 'center',
        src: './assets/images/icon-' + research + '.png'
      }, 'a-image')
      button.setAttribute('trigger-target', '')
      button.addEventListener('triggered', () => {
        if (button === this.activeButton) {
          button.setAttribute('color', colors.off)
          let newPosition = button.getAttribute('position')
          newPosition.y = 0.01
          button.setAttribute('position', newPosition)
          this.activeButton = null
        } else {
          if (this.activeButton) {
            this.activeButton.emit('triggered')
          }
          button.setAttribute('color', colors.on)
          let newPosition = button.getAttribute('position')
          newPosition.y = -0.005
          button.setAttribute('position', newPosition)
          this.activeButton = button
        }
        selectSystem('research').selectResearch(research)
      })
      if (research === 'thrusters') {
        button.setAttribute('trigger-at-start', '')
      }
      this.buttonEls.push(button)
    }
  }
})