AFRAME.registerComponent('value-update', {
  schema: {
    type: { type: 'string', default: null }
  },
  init () {
    this.el.addEventListener('newValue', event => {
      let value = this.el.getAttribute('value').split('/')
      value[0] = event.detail.toLocaleString() + ' '
      this.el.setAttribute('value', value.join('/'))
    })
    this.el.addEventListener('newMaxValue', event => {
      let value = this.el.getAttribute('value').split('/')
      value[1] = ' ' + event.detail.toLocaleString()
      this.el.setAttribute('value', value.join('/'))
    })
  }
})