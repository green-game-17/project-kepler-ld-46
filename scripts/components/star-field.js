AFRAME.registerComponent('star-field', {
    schema: {
        depth: {type: 'number'},
        width: {type: 'number'},
        height: {type: 'number'},
        debug: {type: 'boolean', default: false}
    },

    init: function () {
        this._stars = []

        this.maxStars = 100
        this.velocity = 0.1        

        if (this.data.debug) {
            createEntity(this.el, {
                geometry: {
                    primitive: 'box',
                    depth: this.data.depth,
                    width: this.data.width,
                    height: this.data.height,
                },
                position: `0 0 ${this.data.depth / 2}`
            })
        }
    },
    tick: function () {
        this._stars.forEach(star => {
            star.getAttribute('position').z = star.getAttribute('position').z + this.velocity
        })
        this._stars
            .filter(star => star.getAttribute('position').z >= this.data.depth)
            .map(star => star.parentNode.removeChild(star))
        ;
        this._stars = this._stars.filter(star => star.getAttribute('position').z < this.data.depth)
        

        if (this._stars.length < this.maxStars) {
            if (pickNum(1, 5) == 1) {
                let x = pickFloat(-this.data.width / 2, this.data.width / 2)
                let y = pickFloat(-this.data.height / 2, this.data.height / 2)

                let star = createEntity(this.el, {
                    geometry: { 
                        primitive: 'octahedron', radius: 0.025
                    },
                    material: { 
                        color: '#fcf9d1'
                    },
                    position: `${x} ${y} 0`
                })
                this._stars.push(star)
            }
        }
    }
})
