AFRAME.registerComponent('main-buttons', {
  init () {
    this.distances = {
      x: 0.5,
    }
    this.activeButton = null

    let colors = {
      on: '#4d4f3b',
      off: '#0a0a07'
    }

    this.paths = [
      "./assets/images/icon-wrench.png",
      "./assets/images/icon-drone.png",
      "./assets/images/icon-scanner.png"
    ]

    this.names = [
      "Repair",
      "Drone",
      "Scanner"
    ]

    this.buttonEls = []

    for (let i = 0; i < 3; i++) {
      let x = (this.distances.x * this.buttonEls.length)
      let button = createEntity(this.el, {
        id: 'main' + this.names[i] + 'Button',
        position: x + ' 0.01 0' ,
        depth: '0.2',
        height: '0.033',
        width: '0.4',
        color: colors.off
      }, 'a-box')
      let icon = createEntity(button, {
        id: 'main' + this.names[i] + 'Icon',
        position: '0 0.017 0',
        rotation: '-90 0 0',
        scale: '0.15 0.15 0.15',
        align: 'center',
        src: this.paths[i]
      }, 'a-image')
      button.setAttribute('trigger-target', '')
      button.addEventListener('triggered', () => {
        if (button === this.activeButton) {
          button.setAttribute('color', colors.off)
          let newPosition = button.getAttribute('position')
          newPosition.y = 0.01
          button.setAttribute('position', newPosition)
          this.activeButton = null
        } else {
          if (this.activeButton) {
            this.activeButton.emit('triggered')
          }
          button.setAttribute('color', colors.on)
          let newPosition = button.getAttribute('position')
          newPosition.y = -0.005
          button.setAttribute('position', newPosition)
          this.activeButton = button
        }
      })
      this.buttonEls.push(button)
    }
  }
})