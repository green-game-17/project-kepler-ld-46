AFRAME.registerComponent('storage-list', {
  init () {
    this.storageSystem = selectSystem('storage')

    this.topY = 0.42
    this.distance = 0.16
    this.startIndex = 0
    this.currentSelected = 0
    this.currentSelectedName = ''
    this.space = 5
    this.color = {
      active: '#4d4f3b',
      notActive: '#3e3e3e'
    }
    this.planes = []
    this.refreshList()
    this.storageSystem.registerListener(this.el, 'list', 'list')
    this.el.addEventListener('newSelected', event => {
      if (event.detail > 0) {
        this.currentSelected += event.detail
        if (this.currentSelected >= Object.keys(this.storageSystem.resources).length) {
          this.currentSelected = 0
        }
      } else {
        this.currentSelected += event.detail
        if (this.currentSelected < 0) {
          this.currentSelected = Object.keys(this.storageSystem.resources).length - 1
        }
      }
      if (this.currentSelected < this.startIndex) {
        this.startIndex = this.currentSelected
      } else if (this.currentSelected >= this.startIndex + this.space) {
        this.startIndex = this.currentSelected - this.space + 1
      }
      this.refreshList()
    })
    this.el.addEventListener('deleteResource', () => {
      this.storageSystem.removeMaterial(this.currentSelectedName)
      this.currentSelected = 0
      this.currentSelectedName = ''
      this.startIndex = 0
      this.refreshList()
    })
  },
  refreshList () {
    let counter = 0
    let amount = 0

    for (let child of this.planes) {
      this.el.removeChild(child)
    }
    this.planes = []

    for (let resource in this.storageSystem.resources) {
      if (this.startIndex <= counter && this.startIndex + this.space > counter) {
        if (counter === this.currentSelected) {
          this.currentSelectedName = resource
        }
        let plane = createEntity(this.el, {
          id: 'storage' + resource + 'Content',
          position: '-0.14 ' + (this.topY - (counter - this.startIndex) * this.distance) + ' 0',
          width: '1.17',
          height: '0.15',
          color: counter === this.currentSelected ? this.color.active : this.color.notActive
        }, 'a-plane')
        let label = createEntity(plane, {
          id: 'storage' + resource + 'ContentLabel',
          position: '-0.55 0 0',
          scale: '0.4 0.4 0.4',
          value: translate(resource),
          color: '#eaeaea'
        }, 'a-text')
        let value = createEntity(plane, {
          id: 'storage' + resource + 'ContentValue',
          position: '0.55 0 0',
          scale: '0.4 0.4 0.4',
          value: this.storageSystem.resources[resource].toLocaleString() + ' kg',
          color: '#eaeaea',
          align: 'right'
        }, 'a-text')
        this.planes.push(plane)
      }
      counter++
      amount += this.storageSystem.resources[resource]
    }
    this.storageSystem.newAmount(amount)
  }
})