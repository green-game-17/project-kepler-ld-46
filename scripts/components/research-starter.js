AFRAME.registerComponent('research-starter', {
  init () {
    this.el.addEventListener('triggered', () => {
      selectSystem('research').startResearch()
    })
  }
})