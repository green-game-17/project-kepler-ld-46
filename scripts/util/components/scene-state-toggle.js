AFRAME.registerComponent('scene-state-toggle', {
    init: function () {
        this.menuMusicEl = selectEntity('menuMusicSound')
        this.gameMusicEl = selectEntity('gameMusicSound')

        selectSystem('game-state').registerListener(this)
        this.el.addEventListener('gameStateChanged', ({detail}) => {
            let state = detail.newState

            switch (state) {
                case 'start':
                    this.menuMusicEl.components.sound.stopSound()
                    this.gameMusicEl.components.sound.playSound()
                    break;
                case 'death':
                case 'win':
                    try {
                        this.el.removeChild(selectEntity('root'))
                    } catch {
                        // don't care....
                    }
                    break;
            }
        })
    }
})
