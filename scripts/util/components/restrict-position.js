AFRAME.registerComponent('restrict-position', {
    schema: {
        posX: {type: 'number'},
        negX: {type: 'number'},
        posZ: {type: 'number'},
        negZ: {type: 'number'},
        debug: {type: 'boolean', default: false}
    },

    tick: function () {
        let pos = this._getParentPos()

        if (this.data.debug) {
            console.log(`[C][restrict-position] walked to x=${pos.x} z=${pos.z}`)
            return
        }

        if (selectEntity('scene').is('vr-mode')) {
            return
        }

        if (pos.x > 0) {
            if (pos.x > this.data.posX) { pos.x = this.data.posX }
        } else {
            if (-pos.x > this.data.negX) { pos.x = -this.data.negX }
        }

        if (pos.z > 0) {
            if (pos.z > this.data.posZ) { pos.z = this.data.posZ }
        } else {
            if (-pos.z > this.data.negZ) { pos.z = -this.data.negZ }
        }
    },

    _getParentPos: function () {
        return this.el.getAttribute('position')
    }
})
