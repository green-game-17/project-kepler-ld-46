AFRAME.registerSystem('upgrades', {
  init () {
    this.thrusterLevels = {
      main: 0,
      efficiency: 0
    }
    this.upgradeLevel = {
      thrusters: {
        main: 0,
        efficiency: 0
      },
      scanner: 0,
      storage: 0,
      seats: 0,
      food: 0,
      water: 0,
      energy: 0,
      robotics: 0
    }
    this.upgradeInProgress = false
    this.upgradeData = {
      type: '',
      level: 0
    }

    this._listeners = {}

    this.colors = {
      on: '#228b22',
      off: '#0a0a07'
    }

    let tickInterval = isGreenActivated ? 200 : 1000
    this.tick = AFRAME.utils.throttleTick(this.tick, tickInterval, this)
  },
  tick () {
    if (this.upgradeInProgress) {
      this.finishUpgrade()
    }
  },
  showUpgrade (type, level) {
    if (this.upgradeData.type === type && this.upgradeData.level === level) {
      type = ''
      level = 0
    }
    let unlocked = selectSystem('research').unlocked(type, level + 1)
    if (type === 'thrusters') {
      unlocked = selectSystem('research').unlocked(type, parseInt(level.split('.')[0]) + 1)
    }
    if (type !== '' && unlocked) {
      this.upgradeData = {
        type: type,
        level: level
      }
      this._listeners.materials.list.emit('newMaterials', this.upgradeData)
      this._listeners.text.title.emit('newText', type)
      this._listeners.text.icon.emit('newText', { attribute: 'visible', value: true })
      this._listeners.text.icon.emit('newText', { attribute: 'src', value: './assets/images/icon-' + type + '.png' })
      if (type === 'thrusters') {
        let levels = level.split('.')
        levels[0] = parseInt(levels[0]) + 1
        level = levels.join('.')
      } else {
        level += 1
      }
      this._listeners.text.level.emit('newText', 'Lv. ' + level)
      this._listeners.text.start.emit('newText', this.colors.on)
    } else {
      this.upgradeData = {
        type: '',
        level: 0
      }
      this._listeners.text.title.emit('newText', '')
      this._listeners.materials.list.emit('newMaterials', this.upgradeData)
      this._listeners.text.icon.emit('newText', { attribute: 'visible', value: false })
      this._listeners.text.level.emit('newText', '')
      this._listeners.text.start.emit('newText', this.colors.off)
    }
  },
  startUpgrade () {
    this.upgradeInProgress = true
    let upgradeData = null

    if (this.upgradeData.type === 'thrusters') {
      let levels = this.upgradeData.level.split('.')
      if (parseInt(levels[1]) > 0) {
        upgradeData = upgradesCatalogue[this.upgradeData.type][parseInt(levels[0])].efficiencyUpgrades[parseInt(levels[1]) - 1]
      } else {
        upgradeData = upgradesCatalogue[this.upgradeData.type][parseInt(levels[0])]
      }
    } else {
      upgradeData = upgradesCatalogue[this.upgradeData.type][this.upgradeData.level]
    }
  },
  finishUpgrade () {
    this.upgradeInProgress = false
    console.log(this.upgradeData.level)
    if (this.upgradeData.type === 'thrusters') {
      if (this.upgradeData.level.split('.')[1] === '0') {
        this.finishButton('thrusters', parseInt(this.upgradeData.level.split('.')[0]))
        this.upgradeLevel[this.upgradeData.type].main = parseInt(this.upgradeData.level.split('.')[0])
        this.upgradeLevel[this.upgradeData.type].efficiency = 0
      } else {
        this.finishButton('thrusters', this.upgradeData.level)
        this.upgradeLevel[this.upgradeData.type].main = parseInt(this.upgradeData.level.split('.')[1])
      }
    } else {
      this.finishButton(this.upgradeData.type, this.upgradeData.level)
      this.upgradeLevel[this.upgradeData.type] = this.upgradeData.level
    }
  },
  finishButton (type, level) {
    this._listeners[type][level].emit('finish')
  },
  activateButton (type, level) {
    if (type === 'thrusters') {
      this._listeners[type][level.toString()].emit('activate')
      this._listeners[type][level.toString() + '.1'].emit('activate')
      this._listeners[type][level.toString() + '.2'].emit('activate')
      this._listeners[type][level.toString() + '.3'].emit('activate')
    } else {
      this._listeners[type][level.toString()].emit('activate')
    }
  },
  registerListener (entity, type, id) {
    if (!this._listeners[type]) {
      this._listeners[type] = {}
    }
    this._listeners[type][id] = entity
  }
})