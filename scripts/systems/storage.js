AFRAME.registerSystem('storage', {
  init () {
    this.resources = {
      'aluminium': 9000,
      'copper': 80000,
      'air': 90,
      'ice': 800,
      'iron': 3455,
      'gold': 34927,
      'silver': 99,
      'carbon': 55
    }
    this._listeners = {}
  },
  isEnough (type, value) {
    return this.resources[type] >= value
  },
  substractMaterials (type, value) {
    this.resources[type] -= value
  },
  addMaterials (type, value) {
    if (!this.resources[type]) {
      this.resources[type] = 0
    }
    this.resources[type] += value
  },
  removeMaterial (type) {
    delete this.resources[type]
  },
  moveUp () {
    this._listeners.list.list.emit('newSelected', -1)
  },
  moveDown () {
    this._listeners.list.list.emit('newSelected', 1)
  },
  newAmount (amount) {
    this._listeners.footer.footer.emit('newValue', amount)
  },
  deleteResource () {
    this._listeners.list.list.emit('deleteResource')
  },
  enoughSpace (amount) {
    let stored = 0
    for (let resource in this.resources) {
      stored += this.resources[resource]
    }

    return stored + amount < 500000
  },
  registerListener (entity, type, id) {
    if (!this._listeners[type]) {
      this._listeners[type] = {}
    }
    this._listeners[type][id] = entity
  }
})