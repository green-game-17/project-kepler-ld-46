AFRAME.registerSystem('problems', {
    init: function () {
        this.categories = [ 'thrusters', 'storage', 'spine', 'habitat', 'hangar', 'shield' ]
        this.currentProblems = {
            thrusters: null,
            storage: null,
            spine: null,
            habitat: null,
            hangar: null,
            shield: null,
        },
        this._listeners = {
            thrusters: [],
            storage: [],
            spine: [],
            habitat: [],
            hangar: [],
            shield: [],
            all: [],
        }

        let tickInterval = isGreenActivated ? 5 * 1000 : 3 * 60 * 1000
        this.tick = AFRAME.utils.throttleTick(this.tick, tickInterval, this)

        this._firstSkipped = false
        this._ready = false

        selectSystem('game-state').registerListener(this)
        this.el.addEventListener('gameStateChanged', ({detail}) => {
            if (detail.newState === 'start') {
                this._ready = true
            }
        })
    },
    tick: function () {
        if (!this._ready) {
            return
        }

        if (!this._firstSkipped) {
            this._firstSkipped = true
            return
        }

        if (pickNum(1, 5) !== 1) {
            //return // only trigger on 20% of the time
        }

        let pickedCategory = pick(this.categories)
        if (this.currentProblems[pickedCategory]) {
            console.log(`[S][problems] ${pickedCategory} is already busy being broken`)
            return // only one problem per category at a time
        }

        let pickedProblemTemplate = pick(problemCatalogue[pickedCategory])
        let computedResources = pickedProblemTemplate.resources.map((res) => {
            let id = Array.isArray(res.id) ? pick(res.id) : res.id
            let amount = pickNum(res.minAmount, res.maxAmount)
            return { id: id, amount: amount}
        })
        let pickedProblem = Object.assign(pickedProblemTemplate, { resources: computedResources })

        this._setProblem(pickedCategory, pickedProblem)
    },

    resolveProblem: function (category) {
        this._setProblem(category, null)
    },
    registerListener: function (entity, category) {
        if (!['all', ...this.categories].includes(category)) {
            console.error(`[S][problems] can't add listener - category ${category} unknown`)
            return
        }
        this._listeners[category].push(entity.el)
    },

    _setProblem: function(category, problem) {
        this.currentProblems[category] = problem
        this._listeners[category].forEach(l => l.emit('problemChanged', { category: category, problem: problem }))
        this._listeners['all'].forEach(l => l.emit('problemChanged', { category: category, problem: problem }))
    }
})
