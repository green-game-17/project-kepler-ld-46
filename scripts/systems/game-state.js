AFRAME.registerSystem('game-state', {
    init: function () {
        this._listeners = []

        window.emitGameState = (state, data) => this.set(state, data || {})
    },
    
    set: function (state, data) {
        this._listeners.forEach(l => l.emit('gameStateChanged', { newState: state, data: data }))

        if (state === 'death') {
            window.setDeathScreen(data)
        }
        else if (state === 'win') {
            window.setWinScreen(data)
        }
    },
    registerListener: function (entity) {
        this._listeners.push(entity.el)
    }
})
