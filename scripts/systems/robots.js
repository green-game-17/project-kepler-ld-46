AFRAME.registerSystem('robots', {
  init () {
    this.droneIsActive = false
    this.robotIsActive = false
    this.robotTime = 60
    this.droneTime = 150
    this.robotTimeLeft = 0
    this.droneTimeLeft = 0

    this.robotCost = [
      { id: 'aluminium', amount: 100 },
      { id: 'iron', amount: 80 },
      { id: 'cadmium', amount: 40 },
      { id: 'gold', amount: 20 },
      { id: 'tungsten', amount: 10 }
    ]
    this.droneCost = [
      { id: 'carbon', amount: 450 },
      { id: 'iron', amount: 225 },
      { id: 'gold', amount: 130 },
      { id: 'molybdenum', amount: 65 },
      { id: 'tungsten', amount: 10 }
    ]
  },
  tick () {
    if (this.droneIsActive) {

    }
    if (this.robotIsActive) {

    }
  },
  activate (type) {
    if (type === 'drones') {
      let producable = true
      for (let material of this.droneCost) {
        if (!selectSystem('storage').isEnough(material.id, material.amount)) {
          producable = false
        }
      }
      if (producable) {
        this.startProduction(type)
      }
    } else if (type === 'robots') {
      let producable = true
      for (let material of this.robotCost) {
        if (!selectSystem('storage').isEnough(material.id, material.amount)) {
          producable = false
        }
      }
      if (producable) {
        this.startProduction(type)
      }
    }
  }
})