AFRAME.registerSystem('passengers', {
  init () {
    this.paused = false
    this._listeners = {}
    this.activatedStores = []
    this.maxValues = {
      "reproductionRate": 18,
      "satisfaction": 70,
      "diseaseRisk": 20,
      "communitySpirit": 46,
      "education": 18
    }
    this.currentValues = {
      "reproductionRate": 0.5,
      "satisfaction": 0.5,
      "diseaseRisk": 0.5,
      "communitySpirit": 0.5,
      "education": 0.5
    }

    let tickInterval = isGreenActivated ? 200 : 5 * 1000
    this.tick = AFRAME.utils.throttleTick(this.tick, tickInterval, this)
  },
  pause () {
    this.paused = true
  },
  play () {
    this.paused = false
  },
  tick () {
    if (!this.paused) {
      let addedValues = {
        "reproductionRate": 0,
        "satisfaction": 0,
        "diseaseRisk": 0,
        "communitySpirit": 0,
        "education": 0
      }
      for (let store of this.activatedStores) {
        let storeData = storesCatalogue[store]

        for (let type in storeData) {
          addedValues[type] += storeData[type]
        }
      }

      for (let type in addedValues) {
        this.currentValues[type] += (addedValues[type] / this.maxValues[type] - 0.5) * (this.maxValues[type] / 10000)
        if (this.currentValues[type] < 0) {
          this.currentValues[type] = 0
        } else if (this.currentValues[type] > 1) {
          this.currentValues[type] = 1
        }
      }

      for (let type in this._listeners) {
        for (let id in this._listeners[type]) {
          this._listeners[type][id].emit('newPercentage', Math.round(this.currentValues[id] * 100))
        }
      }
    }
  },
  registerListener (entity, type, id) {
    if (!this._listeners[type]) {
      this._listeners[type] = {}
    }
    this._listeners[type][id] = entity
  }
})