const upgradesCatalogue = {
  "scanner": [
    {
      "resources": [
        { "id": "copper", "amount": 1000 },
        { "id": "carbon", "amount": 2000 },
        { "id": "aluminium", "amount": 4000 },
        { "id": "gallium", "amount": 1000 }
      ]
    }
  ],
  "storage": [
    {
      "resources": [
        { "id": "aluminium", "amount": 350 },
        { "id": "iron", "amount": 250 },
        { "id": "carbon", "amount": 450 }
      ],
      "results": 30
    },
    {
      "resources": [
        { "id": "aluminium", "amount": 1150 },
        { "id": "iron", "amount": 500 },
        { "id": "carbon", "amount": 1500 },
        { "id": "titanium", "amount": 200 }
      ],
      "results": 75
    },
    {
      "resources": [
        { "id": "aluminium", "amount": 2000 },
        { "id": "carbon", "amount": 3000 },
        { "id": "titanium", "amount": 700 }
      ],
      "results": 160
    },
    {
      "resources": [
        { "id": "aluminium", "amount": 4000 },
        { "id": "carbon", "amount": 6500 },
        { "id": "titanium", "amount": 1000 },
        { "id": "beryllium", "amount": 200 }
      ],
      "results": 280
    },
    {
      "resources": [
        { "id": "aluminium", "amount": 6000 },
        { "id": "carbon", "amount": 10000 },
        { "id": "titanium", "amount": 1750 },
        { "id": "beryllium", "amount": 500 }
      ],
      "results": 500
    }
  ],
  "thrusters": [
    {
      "id": "ifThruster",
      "name": "Improved fuel thruster",
      "resources": [
        { "id": "aluminium", "amount": 1000 },
        { "id": "molybdenum", "amount": 1500 },
        { "id": "iron", "amount": 1400 }
      ],
      "efficiencyUpgrades": [
        {
          "id": "ifThrusterE1",
          "resources": [
            { "id": "molybdenum", "amount": 2000 }
          ]
        },
        {
          "id": "ifThrusterE2",
          "resources": [
            { "id": "molybdenum", "amount": 4000 }
          ]
        },
        {
          "id": "ifThrusterE3",
          "resources": [
            { "id": "molybdenum", "amount": 8000 }
          ]
        }
      ]
    },
    {
      "id": "pThruster",
      "name": "Plasma thruster",
      "resources": [
        { "id": "iridium", "amount": 2400 },
        { "id": "tantalum", "amount": 1800 },
        { "id": "copper", "amount": 2100 },
        { "id": "nickel", "amount": 800 }
      ],
      "efficiencyUpgrades": [
        {
          "id": "pThrusterE1",
          "resources": [
            { "id": "tantalum", "amount": 3000 }
          ]
        },
        {
          "id": "pThrusterE2",
          "resources": [
            { "id": "tantalum", "amount": 6000 }
          ]
        },
        {
          "id": "pThruster3",
          "resources": [
            { "id": "tantalum", "amount": 12000 }
          ]
        }
      ]
    },
    {
      "id": "iThruster",
      "name": "Ion thruster",
      "resources": [
        { "id": "copper", "amount": 3800 },
        { "id": "cobalt", "amount": 1900 },
        { "id": "titanium", "amount": 900 }
      ],
      "efficiencyUpgrades": [
        {
          "id": "iThrusterE1",
          "resources": [
            { "id": "cobalt", "amount": 4000 }
          ]
        },
        {
          "id": "iThrusterE2",
          "resources": [
            { "id": "cobalt", "amount": 8000 }
          ]
        },
        {
          "id": "iThrusterE3",
          "resources": [
            { "id": "cobalt", "amount": 16000 }
          ]
        }
      ]
    },
    {
      "id": "nThruster",
      "name": "Nuclear thruster",
      "resources": [
        { "id": "zirconium", "amount": 6000 },
        { "id": "potassium", "amount": 8000 },
        { "id": "sodium", "amount": 7400 },
        { "id": "beryllium", "amount": 3400 }
      ],
      "efficiencyUpgrades": [
        {
          "id": "nThrusterE1",
          "resources": [
            { "id": "zirconium", "amount": 7000 }
          ]
        },
        {
          "id": "nThrusterE2",
          "resources": [
            { "id": "zirconium", "amount": 14000 }
          ]
        },
        {
          "id": "nThruster3",
          "resources": [
            { "id": "zirconium", "amount": 28000 }
          ]
        }
      ]
    },
    {
      "id": "dmThruster",
      "name": "Dark matter thruster",
      "resources": [
        { "id": "tungsten", "amount": 7000 },
        { "id": "chromium", "amount": 7000 },
        { "id": "vanadium", "amount": 5000 }
      ],
      "efficiencyUpgrades": [
        {
          "id": "dmThrusterE1",
          "resources": [
            { "id": "tungsten", "amount": 9000 }
          ]
        },
        {
          "id": "dmThrusterE2",
          "resources": [
            { "id": "tungsten", "amount": 18000 }
          ]
        },
        {
          "id": "dmThrusterE3",
          "resources": [
            { "id": "tungsten", "amount": 36000 }
          ]
        }
      ]
    }
  ],
  "seats": [
    {
      "resources": [
        { "id": "carbon", "amount": 2000 },
        { "id": "iron", "amount": 1300 },
        { "id": "aluminium", "amount": 800 }
      ],
      "needed": [
        { "id": "air", "amount": 1000 }
      ],
      "result": 700
    },
    {
      "resources": [
        { "id": "carbon", "amount": 4000 },
        { "id": "iron", "amount": 2470 },
        { "id": "aluminium", "amount": 1230 }
      ],
      "needed": [
        { "id": "air", "amount": 2500 }
      ],
      "result": 1500
    },
    {
      "resources": [
        { "id": "titanium", "amount": 1650 },
        { "id": "iron", "amount": 3850 },
        { "id": "aluminium", "amount": 2560 }
      ],
      "needed": [
        { "id": "air", "amount": 4300 }
      ],
      "result": 3500
    },
    {
      "resources": [
        { "id": "titanium", "amount": 3250 },
        { "id": "iron", "amount": 5120 },
        { "id": "aluminium", "amount": 3890 }
      ],
      "needed": [
        { "id": "air", "amount": 8200 }
      ],
      "result": 5600
    },
    {
      "resources": [
        { "id": "silver", "amount": 4500 },
        { "id": "iron", "amount": 6990 },
        { "id": "aluminium", "amount": 5270 }
      ],
      "needed": [
        { "id": "air", "amount": 10670 }
      ],
      "result": 8800
    },
    {
      "resources": [
        { "id": "silver", "amount": 7000 },
        { "id": "iron", "amount": 8700 },
        { "id": "aluminium", "amount": 6780 }
      ],
      "needed": [
        { "id": "air", "amount": 14530 }
      ],
      "result": 14400
    },
    {
      "resources": [
        { "id": "gold", "amount": 9700 },
        { "id": "iron", "amount": 9990 },
        { "id": "aluminium", "amount": 7950 }
      ],
      "needed": [
        { "id": "air", "amount": 18340 }
      ],
      "result": 25000
    },
    {
      "resources": [
        { "id": "gold", "amount": 14000 },
        { "id": "iron", "amount": 11110 },
        { "id": "aluminium", "amount": 8670 }
      ],
      "needed": [
        { "id": "air", "amount": 24000 }
      ],
      "result": 37000
    },
    {
      "resources": [
        { "id": "platinum", "amount": 17300 },
        { "id": "iron", "amount": 13310 },
        { "id": "aluminium", "amount": 10000 }
      ],
      "needed": [
        { "id": "air", "amount": 28900 }
      ],
      "result": 49999
    },
    {
      "resources": [
        { "id": "platinum", "amount": 20000 },
        { "id": "iron", "amount": 16000 },
        { "id": "aluminium", "amount": 12000 }
      ],
      "needed": [
        { "id": "air", "amount": 35000 }
      ],
      "result": 100000
    }
  ],
  "food": [
    {
      "resources": [
        { "id": "fertileSoil", "amount": 1000 },
        { "id": "carbon", "amount": 1000 }
      ],
      "result": 5000
    },
    {
      "resources": [
        { "id": "fertileSoil", "amount": 3500 },
        { "id": "carbon", "amount": 3500 }
      ],
      "result": 12000
    },
    {
      "resources": [
        { "id": "fertileSoil", "amount": 6200 },
        { "id": "carbon", "amount": 6200 }
      ],
      "result": 25000
    },
    {
      "resources": [
        { "id": "fertileSoil", "amount": 8400 },
        { "id": "carbon", "amount": 8400 }
      ],
      "result": 45000
    },
    {
      "resources": [
        { "id": "ice", "amount": 6400 },
        { "id": "carbon", "amount": 9400 }
      ],
      "result": 70000
    },
    {
      "resources": [
        { "id": "ice", "amount": 8400 },
        { "id": "carbon", "amount": 11340 }
      ],
      "result": 90000
    },
    {
      "resources": [
        { "id": "ice", "amount": 10000 },
        { "id": "carbon", "amount": 14000 }
      ],
      "result": 100000
    }
  ],
  "water": [
    {
      "resources": [
        { "id": "aluminium", "amount": 750 },
        { "id": "carbon", "amount": 450 },
        { "id": "naturalRubber", "amount": 100 }
      ],
      "result": 35000
    },
    {
      "resources": [
        { "id": "aluminium", "amount": 3200 },
        { "id": "carbon", "amount": 1850 },
        { "id": "naturalRubber", "amount": 420 }
      ],
      "result": 65000
    },
    {
      "resources": [
        { "id": "aluminium", "amount": 7800 },
        { "id": "carbon", "amount": 5350 },
        { "id": "naturalRubber", "amount": 950 }
      ],
      "result": 100000
    }
  ],
  "energy": [
    {
      "resources": [
        { "id": "copper", "amount": 1000 },
        { "id": "carbon", "amount": 500 },
        { "id": "gold", "amount": 50 },
        { "id": "cadmium", "amount": 450 }
      ],
      "result": 50
    },
    {
      "resources": [
        { "id": "copper", "amount": 2300 },
        { "id": "carbon", "amount": 1450 },
        { "id": "gold", "amount": 100 },
        { "id": "cadmium", "amount": 1100 }
      ],
      "result": 150
    },
    {
      "resources": [
        { "id": "copper", "amount": 5300 },
        { "id": "carbon", "amount": 2700 },
        { "id": "gold", "amount": 150 },
        { "id": "iridium", "amount": 150 }
      ],
      "result": 350
    },
    {
      "resources": [
        { "id": "copper", "amount": 8700 },
        { "id": "carbon", "amount": 4000 },
        { "id": "gold", "amount": 225 },
        { "id": "iridium", "amount": 430 }
      ],
      "result": 650
    }
  ],
  "robotics": [
    {
      "resources": [
        { "id": "carbon", "amount": 200 },
        { "id": "iron", "amount": 550 },
        { "id": "aluminium", "amount": 350 }
      ],
      "results": {
        "robots": 50,
        "drones": 50
      }
    },
    {
      "resources": [
        { "id": "carbon", "amount": 950 },
        { "id": "iron", "amount": 1450 },
        { "id": "aluminium", "amount": 1150 }
      ],
      "results": {
        "robots": 140,
        "drones": 100
      }
    },
    {
      "resources": [
        { "id": "carbon", "amount": 1780 },
        { "id": "iron", "amount": 3120 },
        { "id": "aluminium", "amount": 2630 }
      ],
      "results": {
        "robots": 320,
        "drones": 175
      }
    },
    {
      "resources": [
        { "id": "carbon", "amount": 3000 },
        { "id": "iron", "amount": 5040 },
        { "id": "aluminium", "amount": 3430 }
      ],
      "results": {
        "robots": 500,
        "drones": 250
      }
    },
    {
      "resources": [
        { "id": "carbon", "amount": 4500 },
        { "id": "iron", "amount": 7300 },
        { "id": "aluminium", "amount": 4830 }
      ],
      "results": {
        "robots": 820,
        "drones": 375
      }
    },
    {
      "resources": [
        { "id": "carbon", "amount": 8000 },
        { "id": "iron", "amount": 12000 },
        { "id": "aluminium", "amount": 8600 }
      ],
      "results": {
        "robots": 1300,
        "drones": 500
      }
    }
  ]
}