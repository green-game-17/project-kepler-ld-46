const storesCatalogue = {
  "groceryStore": {
    "reproductionRate": 0,
    "satisfaction": 5,
    "diseaseRisk": 0,
    "communitySpirit": 6,
    "education": 0,
    default: true
  },
  "pharmacy": {
    "reproductionRate": -1,
    "satisfaction": 0,
    "diseaseRisk": -5,
    "communitySpirit": 0,
    "education": 0,
    default: true
  },
  "casino": {
    "reproductionRate": -3,
    "satisfaction": 4,
    "diseaseRisk": 0,
    "communitySpirit": -1,
    "education": 0,
    default: true
  },
  "swimmingPool": {
    "reproductionRate": 0,
    "satisfaction": 6,
    "diseaseRisk": 4,
    "communitySpirit": 3,
    "education": 0,
    default: true
  },
  "gym": {
    "reproductionRate": 4,
    "satisfaction": 4,
    "diseaseRisk": -2,
    "communitySpirit": 2,
    "education": 0,
    default: true
  },
  "jewelryStore": {
    "reproductionRate": 2,
    "satisfaction": 2,
    "diseaseRisk": 0,
    "communitySpirit": 1,
    "education": 0
  },
  "cinema": {
    "reproductionRate": 1,
    "satisfaction": 5,
    "diseaseRisk": 0,
    "communitySpirit": 0,
    "education": 0,
    default: true
  },
  "redLightDistrict": {
    "reproductionRate": -5,
    "satisfaction": 5,
    "diseaseRisk": 3,
    "communitySpirit": 0,
    "education": 0
  },
  "culturalCenter": {
    "reproductionRate": 0,
    "satisfaction": 0,
    "diseaseRisk": 0,
    "communitySpirit": 2,
    "education": 2,
    default: true
  },
  "library": {
    "reproductionRate": 0,
    "satisfaction": 1,
    "diseaseRisk": 0,
    "communitySpirit": 0,
    "education": 4
  },
  "church": {
    "reproductionRate": -2,
    "satisfaction": 2,
    "diseaseRisk": 0,
    "communitySpirit": 6,
    "education": 0
  },
  "furnitureShop": {
    "reproductionRate": 0,
    "satisfaction": 3,
    "diseaseRisk": 0,
    "communitySpirit": 0,
    "education": 0
  },
  "police": {
    "reproductionRate": 0,
    "satisfaction": 2,
    "diseaseRisk": 0,
    "communitySpirit": 3,
    "education": 0,
    default: true
  },
  "prison": {
    "reproductionRate": 0,
    "satisfaction": 1,
    "diseaseRisk": 0,
    "communitySpirit": 4,
    "education": 0
  },
  "deliveryService": {
    "reproductionRate": 1,
    "satisfaction": 1,
    "diseaseRisk": 1,
    "communitySpirit": -1,
    "education": 0
  },
  "electronicsStore": {
    "reproductionRate": 0,
    "satisfaction": 3,
    "diseaseRisk": 0,
    "communitySpirit": -1,
    "education": 0
  },
  "restaurants": {
    "reproductionRate": 2,
    "satisfaction": 0,
    "diseaseRisk": 0,
    "communitySpirit": 0,
    "education": 0
  },
  "spa": {
    "reproductionRate": 1,
    "satisfaction": 4,
    "diseaseRisk": 0,
    "communitySpirit": 0,
    "education": 0,
    default: true
  },
  "clothingStore": {
    "reproductionRate": 0,
    "satisfaction": 1,
    "diseaseRisk": 0,
    "communitySpirit": 0,
    "education": 0
  },
  "publicTransportation": {
    "reproductionRate": 0,
    "satisfaction": 2,
    "diseaseRisk": 7,
    "communitySpirit": 0,
    "education": 0,
    default: true
  },
  "park": {
    "reproductionRate": 0,
    "satisfaction": 3,
    "diseaseRisk": 0,
    "communitySpirit": 2,
    "education": 0,
    default: true
  },
  "bar": {
    "reproductionRate": 1,
    "satisfaction": 4,
    "diseaseRisk": 0,
    "communitySpirit": 4,
    "education": 1,
    default: true
  },
  "disco": {
    "reproductionRate": 1,
    "satisfaction": 3,
    "diseaseRisk": 0,
    "communitySpirit": 3,
    "education": 0
  },
  "hospital": {
    "reproductionRate": 5,
    "satisfaction": 2,
    "diseaseRisk": -8,
    "communitySpirit": 0,
    "education": 0,
    default: true
  },
  "educationalInstitutions": {
    "reproductionRate": 0,
    "satisfaction": -3,
    "diseaseRisk": 0,
    "communitySpirit": 2,
    "education": 10,
    default: true
  },
  "cafe": {
    "reproductionRate": 0,
    "satisfaction": 2,
    "diseaseRisk": 0,
    "communitySpirit": 2,
    "education": 1
  },
  "stadium": {
    "reproductionRate": 0,
    "satisfaction": 0,
    "diseaseRisk": 5,
    "communitySpirit": 5,
    "education": 0
  },
  "amusementPark": {
    "reproductionRate": 0,
    "satisfaction": 5,
    "diseaseRisk": 0,
    "communitySpirit": 1,
    "education": 0
  }
}