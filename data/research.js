const researchCatalogue = {
  "scanner": [
    {
      "time": 300,
      "passengers": 10000
    }
  ],
  "storage": [
    {
      "time": 80,
      "passengers": 500
    },
    {
      "time": 165,
      "passengers": 2300
    },
    {
      "time": 260,
      "passengers": 8500
    },
    {
      "time": 510,
      "passengers": 33000
    },
    {
      "time": 900,
      "passengers": 74000
    }
  ],
  "thrusters": [
    {
      "time": 60,
      "passengers": 400
    },
    {
      "time": 230,
      "passengers": 2500
    },
    {
      "time": 465,
      "passengers": 6660
    },
    {
      "time": 817,
      "passengers": 22000
    },
    {
      "time": 1200,
      "passengers": 50000
    }
  ],
  "seats": [
    {
      "time": 40,
      "passengers": 240
    },
    {
      "time": 110,
      "passengers": 666
    },
    {
      "time": 190,
      "passengers": 1400
    },
    {
      "time": 280,
      "passengers": 3333
    },
    {
      "time": 380,
      "passengers": 5300
    },
    {
      "time": 490,
      "passengers": 8300
    },
    {
      "time": 610,
      "passengers": 13500
    },
    {
      "time": 740,
      "passengers": 23500
    },
    {
      "time": 880,
      "passengers": 35000
    },
    {
      "time": 1070,
      "passengers": 45000
    }
  ],
  "food": [
    {
      "time": 120,
      "passengers": 950
    },
    {
      "time": 180,
      "passengers": 4500
    },
    {
      "time": 270,
      "passengers": 11000
    },
    {
      "time": 400,
      "passengers": 23000
    },
    {
      "time": 520,
      "passengers": 44000
    },
    {
      "time": 610,
      "passengers": 69000
    },
    {
      "time": 840,
      "passengers": 89000
    }
  ],
  "water": [
    {
      "time": 180,
      "passengers": 8000
    },
    {
      "time": 360,
      "passengers": 25000
    },
    {
      "time": 600,
      "passengers": 60000
    }
  ],
  "energy": [
    {
      "time": 170,
      "passengers": 7200
    },
    {
      "time": 290,
      "passengers": 18200
    },
    {
      "time": 480,
      "passengers": 36000
    },
    {
      "time": 800,
      "passengers": 77000
    }
  ],
  "robotics": [
    {
      "time": 105,
      "passengers": 1050
    },
    {
      "time": 200,
      "passengers": 5650
    },
    {
      "time": 355,
      "passengers": 10450
    },
    {
      "time": 495,
      "passengers": 21320
    },
    {
      "time": 755,
      "passengers": 39670
    },
    {
      "time": 930,
      "passengers": 58150
    }
  ]
}